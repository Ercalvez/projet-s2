package graphique;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class FenAccueil extends Stage
{
	/**
	 * Titre des catégories pour orienter le client 
	 */
	private Label		mairie		= new Label("Mairie");
	private Label		entreprise	= new Label("Entreprise");
	/**
	 * Orienter le client s'il vient de la mairie
	 */
	private Label		loginMai	= new Label("Entrez votre login de l'entreprise :");
	/**
	 * Entrée du login
	 */
	private TextField login 		= new TextField();
	/**
	 * bouton pour valider, éditer une demande, afficher la carte
	 */
	private Button		bnEditer	= new Button("Editer une demande");
	private Button		bnCarte		= new Button("Afficher la carte");
	private Button		bnValide	= new Button("Valider");
	
	
	/**
	 * Constructeur de la fenetre, fait appel à creerContenu()
	 * @throws FileNotFoundException
	 */
	public FenAccueil()
	{
		//Écran
		Screen screen = Screen.getPrimary();
		//title
		this.setTitle("Accueil");
		
		//size
		/* this.setWidth(1500);
		 this.setHeight(800);*/
		 this.setWidth(1500);
		 this.setHeight(800);
		 this.setResizable(false);
		 
		 //position
		 this.setX(50);
		 this.setY(20);
		 
		 //scene
		 Scene sc1 = new Scene(creerContenuAccueil(), 1200, 800);
		 this.setScene(sc1);
		 this.sizeToScene();
	}
	
	/**
	 * Creer le contenu de la Page d'accueil
	 * @return elements
	 * @throws FileNotFoundException
	 */
	Parent creerContenuAccueil()
	{
		// appel entete
		Entete entete = new Entete();
		
		//color
		 //Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 mairie.setTextFill(orange);
		 entreprise.setTextFill(orange);
		 loginMai.setTextFill(marron);
		 bnEditer.setTextFill(marron);
		 bnEditer.setOnAction(e->{allerEditer(e);});
		 bnCarte.setTextFill(marron);
		 bnCarte.setOnAction(e->{allerCarte(e);});
		 bnValide.setTextFill(marron);
		 bnValide.setOnAction(e->{verifierLogin(e);});
		 try
		 {
		 
		 //font
		 try {
			 System.out.println();
			 final Font f = Font.loadFont(new FileInputStream(new File("src/font/titre.otf")), 40);
			 mairie.setFont(f);
			 entreprise.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("src/font/texte.ttf")), 25);
			 loginMai.setFont(f2);
			 bnEditer.setFont(f2);
			 bnCarte.setFont(f2);
			 bnValide.setFont(f2);
			 login.setFont(f2);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 //image 
		 
		 
		 Image barre = new Image(new FileInputStream(new File("src/img/barre.png")));
		 ImageView barreO = new ImageView(barre);
		 barreO.setFitHeight(550);
		 barreO.setPreserveRatio(true);
		 
		 Image fond = new Image(new FileInputStream(new File("src/img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);
		
		//position element
		 
		 		//fond
		 fondEcran.setLayoutX(0);
		 fondEcran.setLayoutY(150);
		 
		 		//Mes gridPanes
		 GridPane gpAll			= new GridPane();
		 
		 GridPane gpBody 		= new GridPane();
		 GridPane gpBodyMairie	= new GridPane();
		 GridPane gpBodyEnt		= new GridPane();

		 
		 		//gpAll
		 RowConstraints row0 = new RowConstraints();
		 row0.setPercentHeight(25);
		 RowConstraints row1 = new RowConstraints();
		 row1.setPercentHeight(75);
		 gpAll.getRowConstraints().addAll(row0, row1);
		 
		 gpAll.add(entete.getGpHead(),0,0);
		 gpAll.add(gpBody, 0, 1);

		 		
		 		//gpBody
		 gpBody.setAlignment(Pos.BASELINE_LEFT);
		 GridPane.setHalignment(gpBodyMairie, HPos.LEFT);
		 gpBody.setHalignment(barreO, HPos.LEFT);
		 GridPane.setHalignment(gpBodyEnt, HPos.LEFT);
		 
		 ColumnConstraints CBodyCol0 =new ColumnConstraints();
		 CBodyCol0.setPercentWidth(40);
		 ColumnConstraints CBodyCol1 =new ColumnConstraints();
		 CBodyCol1.setPercentWidth(6);
		 ColumnConstraints CBodyCol2 =new ColumnConstraints();
		 CBodyCol2.setPercentWidth(50);
		 gpBody.getColumnConstraints().addAll(CBodyCol0, CBodyCol1, CBodyCol2);
		 
		 gpBody.add(gpBodyMairie, 0, 0);
		 gpBody.add(barreO, 1, 0);
		 gpBody.add(gpBodyEnt, 2, 0);
		 
		 		//gpBodyMairie
		 gpBodyMairie.setAlignment(Pos.BASELINE_LEFT);
		 GridPane.setHalignment(mairie, HPos.CENTER);
		 GridPane.setHalignment(loginMai, HPos.LEFT);
		 GridPane.setHalignment(login, HPos.LEFT);
		 GridPane.setHalignment(bnValide, HPos.RIGHT);
		 gpBodyMairie.setPadding(new Insets(0,0,0,30));
		 
		 RowConstraints CBodyMairieRow0 =new RowConstraints();
		 CBodyMairieRow0.setPercentHeight(20);
		 RowConstraints CBodyMairieRow1 =new RowConstraints();
		 CBodyMairieRow1.setPercentHeight(25);
		 RowConstraints CBodyMairieRow2 =new RowConstraints();
		 CBodyMairieRow2.setPercentHeight(25);
		 RowConstraints CBodyMairieRow3 =new RowConstraints();
		 CBodyMairieRow3.setPercentHeight(30);
		 gpBodyMairie.getRowConstraints().addAll(CBodyMairieRow0, CBodyMairieRow1, CBodyMairieRow2, CBodyMairieRow3);

		 gpBodyMairie.add(mairie,  0,  0);
		 gpBodyMairie.add(loginMai, 0, 1);
		 gpBodyMairie.add(login, 0, 2);
		 gpBodyMairie.add(bnValide, 0, 3);
		 
		 		//gpBodyEnt
		 gpBodyEnt.setAlignment(Pos.BASELINE_LEFT);
		 GridPane.setHalignment(entreprise, HPos.CENTER);
		 GridPane.setHalignment(bnEditer, HPos.CENTER);
		 GridPane.setHalignment(bnCarte, HPos.CENTER);
		 gpBodyEnt.setPadding(new Insets(0,0,0,100));
		 
		 RowConstraints CBodyEntRow0 =new RowConstraints();
		 CBodyEntRow0.setPercentHeight(20);
		 RowConstraints CBodyEntRow1 =new RowConstraints();
		 CBodyEntRow1.setPercentHeight(40);
		 RowConstraints CBodyEntRow2 =new RowConstraints();
		 CBodyEntRow2.setPercentHeight(40);
		 gpBodyEnt.getRowConstraints().addAll(CBodyEntRow0, CBodyEntRow1, CBodyEntRow2);
		 
		 gpBodyEnt.add(entreprise, 0,0);
		 gpBodyEnt.add(bnEditer, 0, 1);
		 gpBodyEnt.add(bnCarte,  0,  2);
		//group
		Group header = new Group();
		header.getChildren().addAll(fondEcran, gpAll);
		return header;
		 }
		 catch(FileNotFoundException e)
		 {
			 System.out.println("image non trouvée");
			 return null;
		 }
	}
	
	private void verifierLogin(ActionEvent e)
	{
		if (this.login.getText().equals("mairie"))
		{
			this.allerMairie();
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION, "Mot de passe incorrect");
			alert.showAndWait();
		}
	}
	
	private void allerEditer(ActionEvent e)
	{
		Main.ouvrirReservation();
	}
	
	private void allerCarte(ActionEvent e)
	{
		Main.ouvrirCarte();
	}
	
	private void allerMairie()
	{
		Main.ouvrirMairie();
	}
	
	
}
