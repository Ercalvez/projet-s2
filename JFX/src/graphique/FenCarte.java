package graphique;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
//import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import panneau.Demande;
import panneau.Emplacement;
import panneau.Panneau;
public class FenCarte extends Stage 
{
	/**
	 * Titre des catégories pour orienter le client 
	 */
	private Label		carte		= new Label("Carte");
	/**
	 * bouton pour éditer une demande
	 */
	private Button		bnEditer	= new Button("Editer une demande");
	
	/**
	 * header contenant toute la page
	 */
	private Group 		header 		= new Group();
	/**
	 * texte et image de la page carte
	 */
	private ImageView 	imgCarte	= new ImageView();
	private Label		panLibre	= new Label("Panneau libre");
	private Label		panPrit		= new Label("Panneau réservé");
	private Date dateDuJour = new Date();
	private HashMap<Integer,Panneau>	tabPanneau		 = new HashMap<Integer,Panneau>();
	private HashSet<ImageView>	tabIcone 				= new HashSet<ImageView>();
	/**
	 * Constructeur de la fenetre, fait appel à creerContenu()
	 * @throws FileNotFoundException
	 */
	public FenCarte()
	{
		//title
		this.setTitle("Carte");
		
		//size
		 this.setWidth(1200);
		 this.setHeight(800);
		 this.setResizable(false);
		 
		 //position
		 this.setX(50);
		 this.setY(20);
		 
		 Panneau pan1 = new Panneau(1,2.0,3.0,new Emplacement(50,50));
		 Panneau pan2 = new Panneau(2,2.0,3.0,new Emplacement(50,30));
		 Panneau pan3 = new Panneau(3,2.0,3.0,new Emplacement(20,10));
		 tabPanneau.putIfAbsent(pan1.getnumPanneau(),pan1);
		 tabPanneau.putIfAbsent(pan2.getnumPanneau(),pan2);
		 tabPanneau.putIfAbsent(pan3.getnumPanneau(),pan3);
		 
		 //scene
		 Scene sc1 = new Scene(creerContenuCarte(), 1200, 800);
		 this.setScene(sc1);
		 this.sizeToScene();
	}
	
	 Parent creerContenuCarte()
	 {
		 
		// appel entete
			Entete entete = new Entete();
			
		//color
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 carte.setTextFill(orange);
		 panLibre.setTextFill(marron);
		 panPrit.setTextFill(marron);
		 bnEditer.setTextFill(marron);
		 try
		 {
		 
		 Image imgcarte = new Image(new FileInputStream(new File("src/img/lannion.png")));
		 imgCarte = new ImageView(imgcarte);
		 imgCarte.setFitWidth(660);
		 imgCarte.setPreserveRatio(true);
		 bnEditer.setOnAction(e->{try {
			editer(e);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}});
		 
		//font
		 try {
			 final Font f = Font.loadFont(new FileInputStream(new File("src/font/titre.otf")), 40);
			 carte.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("src/font/texte.ttf")), 25);
			 panLibre.setFont(f2);
			 panPrit.setFont(f2);
			 bnEditer.setFont(f2);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		//image
		 Image fond = new Image(new FileInputStream(new File("src/img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);
		 
		 Image PanneauPrit = new Image(new FileInputStream(new File("src/img/iconePrise.png")));
		 ImageView Pprit = new ImageView(PanneauPrit);
		 Pprit.setFitHeight(60);
		 Pprit.setPreserveRatio(true);
		
		 Image PanneauLibre = new Image(new FileInputStream(new File("src/img/iconeLibre.png")));
		 ImageView Plibre = new ImageView(PanneauLibre);
		 Plibre.setFitHeight(60);
		 Plibre.setPreserveRatio(true);
		 	
		//actualisation des panneaux réservés aujourd'hui
		 
		this.actualiser();
		 
		//position element
		 
		 	//fond d'ecran
		 fondEcran.setLayoutY(150);
		 
	 		//Mes gridPanes
	 GridPane gpAll			= new GridPane();
	 
	 GridPane gpBody 		  = new GridPane();
	 GridPane gpBodyGauche	  = new GridPane();
	 GridPane gpBodyDroit	  = new GridPane();
	 GridPane gpBodyPanneauLi = new GridPane();
	 GridPane gpBodyPanneauPr = new GridPane();

	 
	 		//gpAll
	 RowConstraints row0 = new RowConstraints();
	 row0.setPercentHeight(20.5);
	 RowConstraints row1 = new RowConstraints();
	 row1.setPercentHeight(79.5);
	 gpAll.getRowConstraints().addAll(row0, row1);
	 
	 gpAll.add(entete.getGpHead(),0,0);
	 gpAll.add(gpBody, 0, 1);
	 
	 		//gpBody
	 gpBody.setAlignment(Pos.CENTER);
	 GridPane.setHalignment(gpBodyGauche, HPos.CENTER);
	 GridPane.setHalignment(gpBodyDroit, HPos.CENTER);
	 
	 ColumnConstraints CBodyCol0 =new ColumnConstraints();
	 CBodyCol0.setPercentWidth(30);
	 ColumnConstraints CBodyCol1 =new ColumnConstraints();
	 CBodyCol1.setPercentWidth(70);
	 gpBody.getColumnConstraints().addAll(CBodyCol0, CBodyCol1);
	 
	 gpBody.add(gpBodyGauche, 0, 0);
	 gpBody.add(gpBodyDroit, 1, 0);
	 
	 		//gpBodyGauche
	 gpBodyGauche.setAlignment(Pos.BASELINE_LEFT);
	 GridPane.setHalignment(carte, HPos.CENTER);
	 GridPane.setHalignment(gpBodyPanneauLi, HPos.LEFT);
	 GridPane.setHalignment(gpBodyPanneauPr, HPos.LEFT);
	 GridPane.setHalignment(bnEditer, HPos.RIGHT);
	 gpBodyGauche.setPadding(new Insets(0,0,0,25));
	 
	 RowConstraints BodyGaucheRow0 =new RowConstraints();
	 BodyGaucheRow0.setPercentHeight(30);
	 RowConstraints BodyGaucheRow1 =new RowConstraints();
	 BodyGaucheRow1.setPercentHeight(15);
	 RowConstraints BodyGaucheRow2 =new RowConstraints();
	 BodyGaucheRow2.setPercentHeight(15);
	 RowConstraints BodyGaucheRow3 =new RowConstraints();
	 BodyGaucheRow3.setPercentHeight(25);
	 gpBodyGauche.getRowConstraints().addAll(BodyGaucheRow0, BodyGaucheRow1, BodyGaucheRow2, BodyGaucheRow3);

	 gpBodyGauche.add(carte,  0,  0);
	 gpBodyGauche.add(gpBodyPanneauLi, 0, 1);
	 gpBodyGauche.add(gpBodyPanneauPr, 0, 2);
	 gpBodyGauche.add(bnEditer, 0, 4);
	 
	 		//gpBodyDroite
	 gpBodyDroit.setAlignment(Pos.BASELINE_LEFT);
	 GridPane.setHalignment(imgCarte, HPos.LEFT);
	 

	 
	 gpBodyDroit.add(imgCarte, 0,0);
	 
	 		//gpBodyPanneauLi
	 GridPane.setHalignment(Plibre, HPos.LEFT);
	 GridPane.setHalignment(panLibre, HPos.LEFT);
	 
	 ColumnConstraints BodyPanCol0 =new ColumnConstraints();
	 BodyPanCol0.setPercentWidth(15);
	 ColumnConstraints BodyPanCol1 =new ColumnConstraints();
	 BodyPanCol1.setPercentWidth(85);
	 gpBodyPanneauLi.getColumnConstraints().addAll(BodyPanCol0, BodyPanCol1);
	 
	 gpBodyPanneauLi.add(Plibre, 0, 0);
	 gpBodyPanneauLi.add(panLibre, 1, 0);
	 
	 		//gpBodyPanneauPr
	 GridPane.setHalignment(Pprit, HPos.LEFT);
	 GridPane.setHalignment(panPrit, HPos.LEFT);

	 
	 gpBodyPanneauPr.getColumnConstraints().addAll(BodyPanCol0, BodyPanCol1);
	 
	 gpBodyPanneauPr.add(Pprit, 0, 0);
	 gpBodyPanneauPr.add(panPrit, 1, 0);
	 
	
	//ajout de tous les gridpanes
	header.getChildren().addAll(fondEcran, gpAll);
	
	return header;
		 }
		catch(FileNotFoundException e)
		 {
			 System.out.println("image non trouv�e");
			 return null;
		 }
		 
	}
	 /*
	  * Ouvre la page de réservation
	  */
	 
	 public void editer(ActionEvent e) throws FileNotFoundException
		{
		 Main.ouvrirReservation();
		}
	 
	 /**
	  * associe le panneau depuis la page réservation
	  * @param demande la demande à associer
	  * @param i numéro de panneau
	  */
	 
	 public void associerPanneau(Demande demande, int i)
	 {
		demande.setPanneau(tabPanneau.get(Integer.valueOf(i)));
		
	 }
	 
	 /**
	  * réserve le panneau depuis la page mairie
	  * @param demande la demande à associer
	  * @param i numéro de panneau
	  */
	 public void reserverPanneau(Demande demande, int i)
	 {
		demande.reservation(tabPanneau.get(Integer.valueOf(i)));
		
	 }
	 
	 /**
	  * @return l'imageView correspond à un panneau réservé ou libre
	  **/
	 public ImageView icone(Panneau panneau)
	 {
		 ImageView icone = new ImageView(); 
		if(panneau.disponible(dateDuJour, dateDuJour))
		{
			try 
			{
				Image image = new Image(new FileInputStream(new File("src/img/libre"+panneau.getnumPanneau()+".png")));
				icone.setImage(image);
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println("Icone non trouvé");
			}
		}
		else
		{
			try 
			{
				Image image = new Image(new FileInputStream(new File("src/img/pris"+panneau.getnumPanneau()+".png")));
				icone.setImage(image);
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println("Icone non trouvé");
			}
		}
		icone.setFitHeight(50);
		icone.setPreserveRatio(true);
		return icone;
	 }
	 
	 /**
	  * 
	  * @param p le panneau à placer
	  * @param carte la carte où placer
	  * @return l'emplacement où placer le panneau
	  */
	 public Emplacement placerPanneau(Panneau p,ImageView carte)
	 {
		 
		 Emplacement placement = new Emplacement(415+ 660*p.getLocalisation().getX()/100 , 150 + 482*p.getLocalisation().getY()/100);
		 System.out.println(carte.getBoundsInParent());
		 System.out.println(placement.getX() + " " + placement.getY());
		 return placement;
	 }
	 /**
	  * 
	  * @return l'ensemble des numéros de panneaux
	  */
	 public Set<Integer> getNumPanneau()
	 {
		 return tabPanneau.keySet();
	 }
	 
	 /**
	  * actualise la page
	  */
	 public void actualiser()
	 {
		 header.getChildren().removeAll(tabIcone);
		 tabIcone = new HashSet<ImageView>();
		 for (Iterator<Integer> iterator = tabPanneau.keySet().iterator(); iterator.hasNext();) 
		 {
			 Panneau p = tabPanneau.get(iterator.next());
			 ImageView imageView = (ImageView) icone(p);
			imageView.setLayoutX(placerPanneau(p, imgCarte).getX());
			imageView.setLayoutY(placerPanneau(p, imgCarte).getY());
			tabIcone.add(imageView);
			System.out.println("fait");
		}
		 System.out.println(tabPanneau.get(Integer.valueOf(1)).getTabDate());
		 header.getChildren().addAll(tabIcone);
		 
	 }
	 /**
	  * Ajoute le panneau à tabPanneau
	  * @param p panneau à ajouter
	  */
	 public void ajouterTabPanneau(Panneau p)
	 {
		 tabPanneau.put(Integer.valueOf(p.getnumPanneau()), p);
	 }
	 
	 /**
	  * 
	  * @return tabPanneau
	  */
	 
	 public HashMap<Integer,Panneau> getTabPanneau()
	 {
		 return tabPanneau;
	 }
	 
}