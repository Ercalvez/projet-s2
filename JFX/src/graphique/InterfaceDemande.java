package graphique;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import panneau.Demande;

public class InterfaceDemande {
	/**
	 * Appel de la demande créer
	 */
	private Demande dem;
	private Button bnValider;
	private Button bnRefuser;
	
	/**
	 * création de l'interface d'une demande
	 * @param d
	 */
	public InterfaceDemande(Demande d){
		this.dem = d;
	}
	
	public GridPane creerInterface(){
		try {
			//recupération des éléments
			Label titre = new Label(this.dem.getNom());
			Label numPan = new Label("N° panneau : " + this.dem.getPanneau().getnumPanneau());
			Label dateDBT = new Label("Date de début : " + this.dem.getDebut().toString());
			Label dateFIN = new Label("Date de fin : " + this.dem.getFin().toString());
			Label resume = new Label("Résumé : " + this.dem.getPanneau().getResume());

			//couleur
			Color orange = Color.web("#ffa200");
			Color marron = Color.web("#664040");
			titre.setTextFill(orange);
			numPan.setTextFill(marron);
			dateDBT.setTextFill(marron);
			dateFIN.setTextFill(marron);
			resume.setTextFill(marron);
			
			//font
			try {
				 final Font f2 = Font.loadFont(new FileInputStream(new File("src/font/texte.ttf")), 25);
				 titre.setFont(f2);
				 numPan.setFont(f2);
				 dateDBT.setFont(f2);
				 dateFIN.setFont(f2);
				 resume.setFont(f2);
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			
		//image 
		Image valider;
		valider = new Image(new FileInputStream(new File("src/img/valider.png")));
		ImageView val = new ImageView(valider);
		val.setFitHeight(50);
		val.setPreserveRatio(true);
		 
		Image refuser;
		refuser = new Image(new FileInputStream(new File("src/img/refuser.png")));
		ImageView ref = new ImageView(refuser);
	    ref.setFitHeight(50);
		ref.setPreserveRatio(true);
		//bouton
		bnValider = new Button();
		bnValider.setGraphic(val);
		bnValider.setOnAction(e->{gererBoutons(e);});
		bnRefuser = new Button();
		bnRefuser.setGraphic(ref);
		bnRefuser.setOnAction(e->{gererBoutons(e);});
		
		//gpDemande
		GridPane gpDemande = new GridPane();
		
		gpDemande.setBorder(new Border(new BorderStroke(orange, BorderStrokeStyle.SOLID, new CornerRadii(10), new BorderWidths(3))));
		gpDemande.setPrefWidth(1150);
		GridPane.setMargin(gpDemande,  new Insets(10, 10, 20, 20));
		//gpDemande.setPrefWidth(Screen.getSize()*9);
		//gpDemande.setPrefHeight(Screen.getSize()*2);
		GridPane.setMargin(gpDemande,  new Insets(80, 80, 80, 80));
		
		ColumnConstraints col0 = new ColumnConstraints();
		col0.setPercentWidth(15);
		ColumnConstraints col1 = new ColumnConstraints();
		col1.setPercentWidth(70);
		ColumnConstraints col2 = new ColumnConstraints();
		col2.setPercentWidth(15);
		gpDemande.getColumnConstraints().addAll(col0, col1, col2);
		
		
		gpDemande.add(titre,0,0);
		gpDemande.add(numPan,1,1);
		gpDemande.add(dateDBT,1,2);
		gpDemande.add(dateFIN,1,3);
		gpDemande.add(resume,1,4);
		gpDemande.add(bnValider, 2, 1);
		gpDemande.add(bnRefuser, 2, 3);
		
		return gpDemande;
		
		} catch (FileNotFoundException e) {
			System.out.println("image non trouvée");
			return null;
		}
	}
	private void gererBoutons(ActionEvent e)
	{
		System.out.println("Demande");
		if (e.getSource()==bnValider)
		{
			Main.accepterLaDemande(this);
			Main.reserverPanneau(this.dem, this.dem.getPanneau().getnumPanneau());
		}
		else if (e.getSource()==bnRefuser)
		{
			Main.refuserLaDemande(this);
		}
	}
	/**
	 * @return la demande
	 */
	public Demande getDemande()
	{
		return dem;
	}

}
