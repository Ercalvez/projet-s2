package graphique;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javafx.application.Application;
import javafx.stage.Stage;
import panneau.Demande;
import panneau.Panneau;

public class Main extends Application
{
	static private FenCarte fcarte = new FenCarte();
	static private FenReservation freservation = new FenReservation();
	static private FenAccueil faccueil = new FenAccueil();
	static private FenMairie fmairie = new FenMairie();
	static private ArrayList<Demande>	lesDemandes		 = new ArrayList<Demande>();
	
	
	public void start(Stage sta1) throws FileNotFoundException 
	{
		Main.ouvrirAccueil();
	}
	
	/**
	 * 
	 * @param demande à associer au panneau
	 * @param i indice du panneau demandé
	 * @throws NullPointerException
	 */
	public static void associerPanneau(Demande demande,int i) throws NullPointerException
	{
		fcarte.associerPanneau(demande, i);
		
	}
	
	/**
	 * 
	 * @param demande à réserver au panneau
	 * @param i indice du panneau à réserver
	 * @throws NullPointerException
	 */
	public static void reserverPanneau(Demande demande,int i) throws NullPointerException
	{
		fcarte.reserverPanneau(demande, i);
		
	}
	
	/**
	 * ajoute la demande au tableau des demadnes du Main
	 * @param demande à ajouter
	 */
	public static void ajouterDemande(Demande demande)
	{
		lesDemandes.add(demande);
		freservation.close();
	}
	
	public static void supprimerDemande(Demande demande)
	{
		lesDemandes.remove(demande);
	}
	
	/**
	 * ouvre la page Carte
	 */
	public static void ouvrirCarte()
	{
		fcarte.actualiser();
		fcarte.show();
	}
	
	/**
	 * ouvre la page réservation
	 */
	public static void ouvrirReservation()
	{
		freservation.show();
	}
	
	/**
	 * ouvre la page accueil 
	 */
	
	public static void ouvrirAccueil()
	{
		faccueil.show();
	}
	
	/**
	 * ouvre la page de la mairie montrant les demandes en cours
	 */
	public static void ouvrirMairie()
	{
		fmairie = new FenMairie();
		fmairie.show();
	}
	
	/**
	 * 
	 * @return les demandes en cours
	 */
	public static ArrayList<Demande> getLesDemandes()
	{
		return Main.lesDemandes;
	}
	
	public static Set<Integer> renvoyerNumPanneau()
	{
		return fcarte.getNumPanneau();
	}
	
	/**
	 * Appelle la méthode d'acceptation de demande de la fenêtre Mairie
	 * @param d demande à accepter
	 */
	public static void accepterLaDemande(InterfaceDemande d)
	{
		fcarte.ajouterTabPanneau(d.getDemande().getPanneau());
		fcarte.actualiser();
		fmairie.accepterDemande(d);
	}
	
	/**
	 * Appelle la méthode de refus de demande de la fenêtre Mairie
	 * @param d demande à refuser
	 */
	public static void refuserLaDemande(InterfaceDemande d)
	{
		fmairie.refuserDemande(d);
	}
	
	/**
	 * 
	 * @return tabPanneau de la page carte
	 */
	public static HashMap<Integer,Panneau> getTabPanneau()
	{
		return fcarte.getTabPanneau();
	}
	
	
	public static void main(String[] args) 
	{
		Application.launch(args);

	}
	
	

}
