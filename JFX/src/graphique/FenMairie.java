package graphique;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import panneau.Demande;
import sun.security.provider.PolicyParser.PrincipalEntry;
public class FenMairie extends Stage 
{
	/**
	 * Titre des catégories pour orienter le client 
	 */
	private Label		demandes	= new Label("Demandes");
	
	
	/**
	 * GridPane et ScrollPane pour afficher les demandes
	 */
	
	private FlowPane fp  = new FlowPane();
	private ScrollPane sc = new ScrollPane();
	/**
	 * élements pour la page Mairie
	 */ 
	private ArrayList<InterfaceDemande> tabInterfaceDemande = new ArrayList<InterfaceDemande>();
	
	/**
	 * Constructeur de la fenetre, fait appel à creerContenu()
	 */
	public FenMairie()
	{
		//Écran
		Screen screen = Screen.getPrimary();
		//title
		this.setTitle("Mairie");
		
		//size
		/* this.setWidth(1500);
		 this.setHeight(800);*/
		 this.setWidth(1200);
		 this.setHeight(800);
		 this.setResizable(true);
		 
		 //position
		 this.setX(50);
		 this.setY(20);
		 
		 //scene
		 Scene sc1 = new Scene(creerContenuMairie(), 1200, 800);
		 this.setScene(sc1);
		 this.sizeToScene();
	}
	
	/**
	 * Creer le contenu de la Page d'accueil
	 * @return elements
	 */
	Parent creerContenuMairie()
	{
	    // appel entete
	    	Entete entete = new Entete();
	    	
	    	//scroll bar
	    	
	    	sc.setVbarPolicy(ScrollBarPolicy.ALWAYS);
	    	sc.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
	    	
	    	this.actualiserDemande();
			
	    	sc.setContent(fp);
	    	sc.setPannable(true);
	    	sc.setVmax(500);
	    	//color
		 Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 demandes.setTextFill(orange);
		 
		 
		//font
		 try {
			 final Font f = Font.loadFont(new FileInputStream(new File("src/font/titre.otf")), 40);
			 demandes.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 
			 final Font f2 = Font.loadFont(new FileInputStream(new File("src/font/texte.ttf")), 25);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 try{
		//image
		 Image fond = new Image(new FileInputStream(new File("src/img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);

		 
		//position element
		 
		 	//fond d'ecran
		 fondEcran.setLayoutY(150);
		 
	 		//Mes gridPanes et ScrollPanes
	 GridPane 	gpAll		  = new GridPane();
	 GridPane 	gpBody 		  = new GridPane();
	 GridPane 	gpBodyScroll  = new GridPane();	
	 
	 		//gpAll
	 RowConstraints row0 = new RowConstraints();
	 row0.setPercentHeight(21);
	 RowConstraints row1 = new RowConstraints();
	 row1.setPercentHeight(79);
	 gpAll.getRowConstraints().addAll(row0, row1);
	 
	 gpAll.add(entete.getGpHead(),0,0);
	 gpAll.add(gpBody, 0, 1);
	 
	 		//gpBody
	 gpBody.setAlignment(Pos.BASELINE_LEFT);
	 GridPane.setHalignment(demandes, HPos.CENTER);
	 GridPane.setHalignment(gpBodyScroll, HPos.LEFT);
	 gpBodyScroll.setPadding(new Insets(0,0,0,50));

	 RowConstraints BodyRow0 = new RowConstraints();
	 BodyRow0.setPercentHeight(15);
	 RowConstraints BodyRow1 = new RowConstraints();
	 BodyRow1.setPercentHeight(85);
	 gpBody.getRowConstraints().addAll(BodyRow0, BodyRow1);
	 
	 ColumnConstraints BodyCol = new ColumnConstraints();
	 BodyCol.setPercentWidth(80);
	 gpBody.getColumnConstraints().addAll(BodyCol);
	 
	 gpBody.add(demandes, 0, 0);
	 gpBody.add(sc, 0, 1);
	 
	 		//gpBodyScroll
	 gpBodyScroll.setAlignment(Pos.BASELINE_LEFT);
	 
	//group
	Group header = new Group();
	header.getChildren().addAll(fondEcran, gpAll);
		return header;
	}
		 catch(FileNotFoundException e)
		 {
			 System.out.println("image non trouvée");
			 return null;
		 }
	}
	/**
	 * actualise la liste des demandes en cours
	 */
	
	public void actualiserDemande()
	{
		fp = new FlowPane();
		int i=0;
		if (!(Main.getLesDemandes() == null))
		{	
			for (Iterator<Demande> iterator = Main.getLesDemandes().iterator();iterator.hasNext();)
			{
				InterfaceDemande interfaceDemande = new InterfaceDemande((Demande) iterator.next());
				tabInterfaceDemande.add(interfaceDemande);
				fp.getChildren().add(interfaceDemande.creerInterface());
				i++;
			}
		}
		System.out.println(i);
		fp.setPrefWrapLength(200);
		fp.setVgap(50);
		fp.setPrefHeight(500);
		sc.setContent(fp);
	}
	
	/**
	 * accepte la demande
	 * @param d la demande à accepter
	 */
	
	public void accepterDemande(InterfaceDemande d)
	{
		tabInterfaceDemande.remove(d);
		Main.supprimerDemande(d.getDemande());
		this.actualiserDemande();
		
	
	}
	
	/**
	 * refuse la demande
	 * @param d la demande à refuser
	 */
	
	public void refuserDemande(InterfaceDemande d)
	{
		tabInterfaceDemande.remove(d);
		Main.supprimerDemande(d.getDemande());
		this.actualiserDemande();
	}
	
}
