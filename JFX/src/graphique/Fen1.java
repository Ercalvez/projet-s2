package graphique;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;

import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import panneau.Demande;
import panneau.Emplacement;
import panneau.Panneau;
public class Fen1 extends Stage 
{
	/**
	 * Titre des catégories pour orienter le client 
	 */
	private Label		mairie		= new Label("Mairie");
	private Label		entreprise	= new Label("Entreprise");
	private Label		carte		= new Label("Carte");
	private Label		reserv		= new Label("Réservation");
	/**
	 * Orienter le client s'il vient de la mairie
	 */
	private Label		loginMai	= new Label("Entrez votre login de l'entreprise :");
	/**
	 * Entrée du login
	 */
	private TextField login 		= new TextField();
	/**
	 * bouton pour valider, éditer une demande, afficher la carte
	 */
	private Button		bnEditer	= new Button("Editer une demande");
	private Button		bnCarte		= new Button("Afficher la carte");
	private Button		bnValide	= new Button("Valider");
	
	/**
	 * texte et image de la page carte
	 */
	private Rectangle	imgCarte	= new Rectangle(700, 625);
	private Label		panLibre	= new Label("Panneau libre");
	private Label		panPrit		= new Label("Panneau réservé");
	private Label		detailsPan	= new Label("Cliquez sur un panneau pour plus de d�tails.");
	
	/**
	 * élements pour la page reservation
	 */ 
	private Label 				nomEnt			 = new Label("Nom de l'entreprise :");
	private TextField			nomEntField	 	 = new TextField();
	private Label 				addrEnt			 = new Label("Adresse (localisation) :");
	private TextField			addrEntField	 = new TextField();
	private Label 				addrMailEnt		 = new Label("Adresse Mail :");		private TextField	addrMailEntField = new TextField();
	private Label				numPan			 = new Label("Num�ro du panneau s�lectionn� :");
	private TextField			numPanField 	 = new TextField();
	private Label				resumePan		 = new Label("R�sum� de la publicit� :");
	private TextField			resumePanField 	 = new TextField();
	private Label				dateDebut		 = new Label("Date de d�but de la r�servation :");
	private Label				dateFin			 = new Label("Date de fin de la r�servation :");
	private Button				bnValEd			 = new Button("Valider");
	private HashMap<Integer,Panneau>	tabPanneau		 = new HashMap<Integer,Panneau>();
	
	/**
	 * Constructeur de la fenetre, fait appel à creerContenu()
	 * @throws FileNotFoundException
	 */
	public Fen1() throws FileNotFoundException
	{
		//Écran
		Screen screen = Screen.getPrimary();
		//title
		this.setTitle("Test numéro 1");
		
		//size
		/* this.setWidth(1500);
		 this.setHeight(800);*/
		 this.setWidth(1300);
		 this.setHeight(700);
		 this.setResizable(true);
		 
		 //position
		 this.setX(50);
		 this.setY(20);
		 
		 //scene
		 Scene sc1 = new Scene(creerContenuCarte(), screen.getBounds().getWidth(), screen.getBounds().getHeight());
		 this.setScene(sc1);
		 this.sizeToScene();
	}
	
	/**
	 * Creer le contenu de la Page d'accueil
	 * @return elements
	 * @throws FileNotFoundException
	 */
	Parent creerContenuAccueil() throws FileNotFoundException {
		// appel entete
		Entete entete = new Entete();
		
		//color
		 Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 mairie.setTextFill(orange);
		 entreprise.setTextFill(orange);
		 loginMai.setTextFill(marron);
		 bnEditer.setTextFill(marron);
		 bnCarte.setTextFill(marron);
		 bnValide.setTextFill(marron);
		 
		 
		 //font
		 try {
			 System.out.println();
			 final Font f = Font.loadFont(new FileInputStream(new File("src/font/titre.otf")), 50);
			 
			 mairie.setFont(f);
			 entreprise.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("src/font/texte.ttf")), 30);
			 loginMai.setFont(f2);
			 bnEditer.setFont(f2);
			 bnCarte.setFont(f2);
			 bnValide.setFont(f2);
			 login.setFont(f2);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 //image 
		 Image barre = new Image(new FileInputStream(new File("src/img/barre.PNG")));
		 ImageView barreO = new ImageView(barre);
		 barreO.setFitHeight(550);
		 barreO.setPreserveRatio(true);
		 
		 Image fond = new Image(new FileInputStream(new File("src/img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);
		
		//position element
		 
		 		//fond
		 fondEcran.setLayoutX(0);
		 fondEcran.setLayoutY(150);
		 
		 		//Mes gridPanes
		 GridPane gpAll			= new GridPane();
		 
		 GridPane gpBody 		= new GridPane();
		 GridPane gpBodyMairie	= new GridPane();
		 GridPane gpBodyEnt		= new GridPane();

		 
		 		//gpAll
		 RowConstraints row0 = new RowConstraints();
		 row0.setPercentHeight(25);
		 RowConstraints row1 = new RowConstraints();
		 row1.setPercentHeight(75);
		 gpAll.getRowConstraints().addAll(row0, row1);
		 
		 gpAll.add(entete.getGpHead(),0,0);
		 
		 		
		 		//gpBody
		 gpBody.setAlignment(Pos.CENTER);
		 gpBody.setHalignment(gpBodyMairie, HPos.CENTER);
		 gpBody.setHalignment(gpBodyEnt, HPos.CENTER);
		 ColumnConstraints CBodyCol0 =new ColumnConstraints();
		 CBodyCol0.setPercentWidth(47);
		 ColumnConstraints CBodyCol1 =new ColumnConstraints();
		 CBodyCol1.setPercentWidth(6);
		 ColumnConstraints CBodyCol2 =new ColumnConstraints();
		 CBodyCol2.setPercentWidth(47);
		 gpBody.getColumnConstraints().addAll(CBodyCol0, CBodyCol1, CBodyCol2);
		 
		 gpBody.add(gpBodyMairie, 0, 0);
		 gpBody.add(barreO, 1, 0);
		 gpBody.add(gpBodyEnt, 2, 0);
		 gpBody.setGridLinesVisible(true);
		 gpAll.add(gpBody, 0, 1);
		 
		 		//gpBodyMairie
		 gpBodyMairie.setAlignment(Pos.CENTER);
		 gpBodyMairie.setHalignment(mairie, HPos.CENTER);
		 gpBodyMairie.setHalignment(loginMai, HPos.CENTER);
		 gpBodyMairie.setHalignment(login, HPos.CENTER);
		 gpBodyMairie.setHalignment(bnValide, HPos.CENTER);
		 
		 RowConstraints CBodyMairieRow0 =new RowConstraints();
		 CBodyMairieRow0.setPercentHeight(20);
		 RowConstraints CBodyMairieRow1 =new RowConstraints();
		 CBodyMairieRow1.setPercentHeight(25);
		 RowConstraints CBodyMairieRow2 =new RowConstraints();
		 CBodyMairieRow2.setPercentHeight(25);
		 RowConstraints CBodyMairieRow3 =new RowConstraints();
		 CBodyMairieRow3.setPercentHeight(30);
		 gpBodyMairie.getRowConstraints().addAll(CBodyMairieRow0, CBodyMairieRow1, CBodyMairieRow2, CBodyMairieRow3);

		 gpBodyMairie.add(mairie,  0,  0);
		 gpBodyMairie.add(loginMai, 0, 1);
		 gpBodyMairie.add(login, 0, 2);
		 gpBodyMairie.add(bnValide, 0, 3);
		 
		 		//gpBodyEnt
		 gpBodyEnt.setAlignment(Pos.CENTER);
		 GridPane.setHalignment(entreprise, HPos.CENTER);
		 GridPane.setHalignment(bnEditer, HPos.CENTER);
		 GridPane.setHalignment(bnCarte, HPos.CENTER);
		 
		 RowConstraints CBodyEntRow0 =new RowConstraints();
		 CBodyEntRow0.setPercentHeight(20);
		 RowConstraints CBodyEntRow1 =new RowConstraints();
		 CBodyEntRow1.setPercentHeight(40);
		 RowConstraints CBodyEntRow2 =new RowConstraints();
		 CBodyEntRow2.setPercentHeight(40);
		 gpBodyEnt.getRowConstraints().addAll(CBodyEntRow0, CBodyEntRow1, CBodyEntRow2);
		 
		 gpBodyEnt.add(entreprise, 0,0);
		 gpBodyEnt.add(bnEditer, 0, 1);
		 gpBodyEnt.add(bnCarte,  0,  2);
		//group
		Group header = new Group();
		header.getChildren().addAll(fondEcran, gpAll);
		return header;
	}
	
	/**
	 * Creer le contenu de la Page Carte
	 * @return elements
	 * @throws FileNotFoundException
	 */
	 Parent creerContenuCarte() throws FileNotFoundException 
	 {
		 Panneau pan1 = new Panneau(1,2.0,3.0,new Emplacement(1,1));
		 Panneau pan2 = new Panneau(2,2.0,3.0,new Emplacement(1,2));
		 Panneau pan3 = new Panneau(3,2.0,3.0,new Emplacement(2,1));
		 tabPanneau.putIfAbsent(pan1.getnumPanneau(),pan1);
		 tabPanneau.putIfAbsent(pan2.getnumPanneau(),pan2);
		 tabPanneau.putIfAbsent(pan3.getnumPanneau(),pan3);
		// appel entete
			Entete entete = new Entete();
			
		//color
		 Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 carte.setTextFill(orange);
		 panLibre.setTextFill(marron);
		 panPrit.setTextFill(marron);
		 detailsPan.setTextFill(marron);
		 bnEditer.setTextFill(marron);
		 imgCarte.setFill(marron);
		 bnEditer.setOnAction(e->{try {
			editer(e);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}});
		 
		//font
		 try {
			 final Font f = Font.loadFont(new FileInputStream(new File("src/font/titre.otf")), 50);
			 carte.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("src/font/texte.ttf")), 30);
			 panLibre.setFont(f2);
			 panPrit.setFont(f2);
			 detailsPan.setFont(f2);
			 bnEditer.setFont(f2);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		//image
		 Image fond = new Image(new FileInputStream(new File("src/img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);
		 
		 Image PanneauPrit = new Image(new FileInputStream(new File("src/img/iconePrise.png")));
		 ImageView Pprit = new ImageView(PanneauPrit);
		 Pprit.setFitHeight(75);
		 Pprit.setPreserveRatio(true);
		 
		 
		 Image PanneauLibre = new Image(new FileInputStream(new File("src/img/iconeLibre.png")));
		 ImageView Plibre = new ImageView(PanneauLibre);
		 Plibre.setFitHeight(75);
		 Plibre.setPreserveRatio(true);
		 
		//position element
		 
		 	//fond d'ecran
		 fondEcran.setLayoutY(150);
		 
	 		//Mes gridPanes
	 GridPane gpAll			= new GridPane();
	 
	 GridPane gpBody 		  = new GridPane();
	 GridPane gpBodyGauche	  = new GridPane();
	 GridPane gpBodyDroit	  = new GridPane();
	 GridPane gpBodyPanneauLi = new GridPane();
	 GridPane gpBodyPanneauPr = new GridPane();

	 
	 		//gpAll
	 RowConstraints row0 = new RowConstraints();
	 row0.setPercentHeight(20.5);
	 RowConstraints row1 = new RowConstraints();
	 row1.setPercentHeight(79.5);
	 gpAll.getRowConstraints().addAll(row0, row1);
	 
	 gpAll.add(entete.getGpHead(),0,0);
	 gpAll.add(gpBody, 0, 1);
	 
	 		//gpBody
	 gpBody.setAlignment(Pos.CENTER);
	 gpBody.setHalignment(gpBodyGauche, HPos.CENTER);
	 gpBody.setHalignment(gpBodyDroit, HPos.CENTER);
	 
	 ColumnConstraints CBodyCol0 =new ColumnConstraints();
	 CBodyCol0.setPercentWidth(45);
	 ColumnConstraints CBodyCol1 =new ColumnConstraints();
	 CBodyCol1.setPercentWidth(55);
	 gpBody.getColumnConstraints().addAll(CBodyCol0, CBodyCol1);
	 
	 gpBody.add(gpBodyGauche, 0, 0);
	 gpBody.add(gpBodyDroit, 1, 0);
	 
	 		//gpBodyGauche
	 gpBodyGauche.setAlignment(Pos.CENTER);
	 gpBodyGauche.setHalignment(carte, HPos.CENTER);
	 gpBodyDroit.setHalignment(gpBodyPanneauLi, HPos.LEFT);
	 gpBodyDroit.setHalignment(gpBodyPanneauPr, HPos.LEFT);
	 gpBodyDroit.setHalignment(bnEditer, HPos.CENTER);

	 RowConstraints BodyGaucheRow0 =new RowConstraints();
	 BodyGaucheRow0.setPercentHeight(30);
	 RowConstraints BodyGaucheRow1 =new RowConstraints();
	 BodyGaucheRow1.setPercentHeight(15);
	 RowConstraints BodyGaucheRow2 =new RowConstraints();
	 BodyGaucheRow2.setPercentHeight(15);
	 RowConstraints BodyGaucheRow3 =new RowConstraints();
	 BodyGaucheRow3.setPercentHeight(15);
	 RowConstraints BodyGaucheRow4 =new RowConstraints();
	 BodyGaucheRow4.setPercentHeight(25);
	 gpBodyGauche.getRowConstraints().addAll(BodyGaucheRow0, BodyGaucheRow1, BodyGaucheRow2, BodyGaucheRow3, BodyGaucheRow4);

	 gpBodyGauche.add(carte,  0,  0);
	 gpBodyGauche.add(gpBodyPanneauLi, 0, 1);
	 gpBodyGauche.add(gpBodyPanneauPr, 0, 2);
	 gpBodyGauche.add(detailsPan, 0, 3);
	 gpBodyGauche.add(bnEditer, 0, 4);
	 
	 		//gpBodyDroite
	 gpBodyDroit.setAlignment(Pos.CENTER);
	 gpBodyDroit.setHalignment(imgCarte, HPos.CENTER);
	 
	 gpBodyDroit.add(imgCarte, 0,0);
	 
	 		//gpBodyPanneauLi
	 gpBodyPanneauLi.setHalignment(Plibre, HPos.LEFT);
	 gpBodyPanneauLi.setHalignment(panLibre, HPos.LEFT);
	 
	 ColumnConstraints BodyPanCol0 =new ColumnConstraints();
	 BodyPanCol0.setPercentWidth(15);
	 ColumnConstraints BodyPanCol1 =new ColumnConstraints();
	 BodyPanCol1.setPercentWidth(85);
	 gpBodyPanneauLi.getColumnConstraints().addAll(BodyPanCol0, BodyPanCol1);
	 
	 gpBodyPanneauLi.add(Plibre, 0, 0);
	 gpBodyPanneauLi.add(panLibre, 1, 0);
	 
	 		//gpBodyPanneauPr
	 gpBodyPanneauPr.setHalignment(Pprit, HPos.LEFT);
	 gpBodyPanneauPr.setHalignment(panPrit, HPos.LEFT);

	 
	 gpBodyPanneauPr.getColumnConstraints().addAll(BodyPanCol0, BodyPanCol1);
	 
	 gpBodyPanneauPr.add(Pprit, 0, 0);
	 gpBodyPanneauPr.add(panPrit, 1, 0);
	 
	//group
	Group header = new Group();
	header.getChildren().addAll(fondEcran, gpAll);
		return header;
	}

	 Parent creerContenuReservation() throws FileNotFoundException {
		 	// appel entete
			Entete entete = new Entete();
		 
			//gestion des calendriers de reservation
			 DatePicker calResDeb = new DatePicker();
			 calResDeb.setValue(LocalDate.now());
			 calResDeb.setShowWeekNumbers(true);
			 
			 DatePicker calResFin = new DatePicker();
			 calResFin.setValue(calResDeb.getValue());
			 calResFin.setShowWeekNumbers(true);
			 
			//color
			 Color rose = Color.web("#ff0046");
			 Color orange = Color.web("#ffa200");
			 Color marron = Color.web("#664040");
			 reserv.setTextFill(orange);
			 nomEnt.setTextFill(marron);
			 addrEnt.setTextFill(marron);
			 addrMailEnt.setTextFill(marron);
			 numPan.setTextFill(marron);
			 resumePan.setTextFill(marron);
			 dateDebut.setTextFill(marron);
			 dateFin.setTextFill(marron);
			 bnValEd.setTextFill(marron);
			 
			//font
			 try {
				 final Font f = Font.loadFont(new FileInputStream(new File("./src/font/titre.otf")), 50);
				 reserv.setFont(f);
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			 
			 try {
				 final Font f2 = Font.loadFont(new FileInputStream(new File("./src/font/texte.ttf")), 25);
				 nomEnt.setFont(f2);
				 nomEntField.setFont(f2);
				 addrEnt.setFont(f2);
				 addrEntField.setFont(f2);
				 addrMailEnt.setFont(f2);
				 addrMailEntField.setFont(f2);
				 numPan.setFont(f2);
				 numPanField.setFont(f2);
				 resumePan.setFont(f2);
				 resumePanField.setFont(f2);
				 dateDebut.setFont(f2);
				 dateFin.setFont(f2);
				 bnValEd.setFont(f2);
				 bnValEd.setOnAction(e ->{reserver(e,calResDeb,calResFin);});
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			 
			 
			//image
			 Image logoR = new Image(new FileInputStream(new File("./src/img/logoR.PNG")));
			 ImageView logoHead = new ImageView(logoR);
			 logoHead.setFitHeight(150);
			 logoHead.setPreserveRatio(true);
			 
			 Image fond = new Image(new FileInputStream(new File("./src/img/fond.PNG")));
			 ImageView fondEcran = new ImageView(fond);
			 fondEcran.setFitHeight(650);
			 fondEcran.setPreserveRatio(true);
	
			 
			//position element
			 
			 	//fond d'ecran
			 fondEcran.setLayoutY(150);
			 
		 		//Mes gridPanes et ScrollPanes
		 GridPane 	gpAll		  = new GridPane();
		 
		 GridPane 	gpBody 		  = new GridPane();
		 GridPane	gpBodyQues	  = new GridPane();

		 
		 		//gpAll
		 RowConstraints row0 = new RowConstraints();
		 row0.setPercentHeight(21);
		 RowConstraints row1 = new RowConstraints();
		 row1.setPercentHeight(79);
		 gpAll.getRowConstraints().addAll(row0, row1);
		 
		 gpAll.add(entete.getGpHead(),0,0);
		 gpAll.add(gpBody, 0, 1);
		 
		 		//gpBody
		 gpBody.setAlignment(Pos.CENTER);
		 gpBody.setHalignment(reserv, HPos.CENTER);
		 gpBody.setHalignment(gpBodyQues, HPos.LEFT);
		 
		 RowConstraints BodyRow0 = new RowConstraints();
		 BodyRow0.setPercentHeight(15);
		 RowConstraints BodyRow1 = new RowConstraints();
		 BodyRow1.setPercentHeight(85);
		 gpBody.getRowConstraints().addAll(BodyRow0, BodyRow1);
		 
		 ColumnConstraints BodyCol = new ColumnConstraints();
		 BodyCol.setPercentWidth(100);
		 gpBody.getColumnConstraints().addAll(BodyCol);
		 
		 gpBody.add(reserv, 0, 0);
		 gpBody.add(gpBodyQues, 0, 1);
		 
		 		//gpBodyQues
		 ColumnConstraints ColQues0 = new ColumnConstraints();
		 ColQues0.setPercentWidth(30);
		 ColumnConstraints ColQues1 = new ColumnConstraints();
		 ColQues1.setPercentWidth(30);
		 gpBodyQues.getColumnConstraints().addAll(ColQues0, ColQues1);
		 
		 RowConstraints RowQues0 = new RowConstraints();
		 RowQues0.setPercentHeight(13);
		 RowConstraints RowQues1 = new RowConstraints();
		 RowQues1.setPercentHeight(13);
		 RowConstraints RowQues2 = new RowConstraints();
		 RowQues2.setPercentHeight(13);
		 RowConstraints RowQues3 = new RowConstraints();
		 RowQues3.setPercentHeight(13);
		 RowConstraints RowQues4 = new RowConstraints();
		 RowQues4.setPercentHeight(13);
		 RowConstraints RowQues5 = new RowConstraints();
		 RowQues5.setPercentHeight(13);
		 RowConstraints RowQues6 = new RowConstraints();
		 RowQues6.setPercentHeight(13);
		 RowConstraints RowQues7 = new RowConstraints();
		 RowQues7.setPercentHeight(13);
		 gpBodyQues.getRowConstraints().addAll(RowQues0, RowQues1, RowQues2, RowQues3, RowQues4, RowQues5, RowQues6,RowQues7);
		 
		 gpBodyQues.setAlignment(Pos.CENTER);
		 gpBodyQues.add(nomEnt, 0, 0);
		 gpBodyQues.add(nomEntField, 1, 0);
		 
		 gpBodyQues.add(addrEnt, 0, 1);
		 gpBodyQues.add(addrEntField, 1, 1);
		 
		 gpBodyQues.add(addrMailEnt, 0, 2);
		 gpBodyQues.add(addrMailEntField, 1, 2);
		 
		 gpBodyQues.add(numPan, 0, 3);
		 gpBodyQues.add(numPanField, 1, 3);
		 
		 gpBodyQues.add(resumePan, 0, 4);
		 gpBodyQues.add(resumePanField, 1, 4);
		 
		 gpBodyQues.add(dateDebut, 0, 5);
		 gpBodyQues.add(calResDeb, 1, 5);
		 
		 gpBodyQues.add(dateFin, 0, 6);
		 gpBodyQues.add(calResFin, 1, 6);
		 
		 gpBodyQues.add(bnValEd, 3, 7);
		 
		//group
		Group header = new Group();
		header.getChildren().addAll(fondEcran, gpAll);
			return header;
		}
	 

	 /**
	  * Récupère les informations saisies et crée la demande
	  * @param e evenement du clic sur le bouton valider
	  * @param CalDeb date de début choisie dans le calendrier
	  * @param CalFin date de fin choisie dans le calendrier
	  **/
	 private void reserver(ActionEvent e,DatePicker CalDeb,DatePicker CalFin)
	 {
		 Demande maDemande = new Demande(nomEntField.getText(),Date.valueOf(CalDeb.getValue()),Date.valueOf(CalFin.getValue()));
		 System.out.println(maDemande.getNom());
		 maDemande.reservation(tabPanneau.get(Integer.valueOf(numPanField.getText())));
		 System.out.println(maDemande.getPanneau().getnumPanneau());
		 
	 }
	 
	 public void editer(ActionEvent e) throws FileNotFoundException
		{
		 Stage sta1 = new Fen1();
		 Scene sc1 = new Scene(creerContenuReservation(), 500, 500);
		 sta1.setScene(sc1);
		 sta1.sizeToScene();
		 sta1.show();
		}
	 
	 }
	 
	
