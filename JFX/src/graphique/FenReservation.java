package graphique;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import panneau.Demande;

public class FenReservation extends Stage
{
	private Label		reserv		= new Label("Réservation");

	/**
	 * élements pour la page reservation
	 */ 
	private Label 				nomEnt			 = new Label("Nom de l'entreprise :");
	private TextField			nomEntField	 	 = new TextField();
	private Label 				addrEnt			 = new Label("Adresse (localisation) :");
	private TextField			addrEntField	 = new TextField();
	private Label 				addrMailEnt		 = new Label("Adresse Mail :");		
	private TextField			addrMailEntField = new TextField();
	private Label				numPan			 = new Label("Numéro du panneau sélectionné :");
	private ChoiceBox<Integer> 		numPanField		 = new ChoiceBox<Integer>();
	//private TextField			numPanField 	 = new TextField();
	private Label				resumePan		 = new Label("Résumé de la publicité :");
	private TextField			resumePanField 	 = new TextField();
	private Label				dateDebut		 = new Label("Date de début de la réservation :");
	private Label				dateFin			 = new Label("Date de fin de la réservation :");
	private Button				bnValEd			 = new Button("Valider");
	private Demande				laDemande 		 = null;
	

	public FenReservation()
	{
		//Écran
		Screen screen = Screen.getPrimary();
		//title
		this.setTitle("Réservation");
		
		//size
		/* this.setWidth(1500);
		 this.setHeight(800);*/
		 this.setWidth(1300);
		 this.setHeight(700);
		 this.setResizable(true);
		 
		 //position
		 this.setX(50);
		 this.setY(20);
		 
		 //scene
		 Scene sc1 = new Scene(creerContenuReservation(), 1200, 800);
		 this.setScene(sc1);
		 this.sizeToScene();
	}
	Parent creerContenuReservation() 
	{
	 	// appel entete
		Entete entete = new Entete();
	 
		//gestion des calendriers de reservation
		 DatePicker calResDeb = new DatePicker();
		 calResDeb.setValue(LocalDate.now());
		 calResDeb.setShowWeekNumbers(true);
		 
		 DatePicker calResFin = new DatePicker();
		 calResFin.setValue(calResDeb.getValue());
		 calResFin.setShowWeekNumbers(true);
		 
		//color
		 //Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 reserv.setTextFill(orange);
		 nomEnt.setTextFill(marron);
		 addrEnt.setTextFill(marron);
		 addrMailEnt.setTextFill(marron);
		 numPan.setTextFill(marron);
		 resumePan.setTextFill(marron);
		 dateDebut.setTextFill(marron);
		 dateFin.setTextFill(marron);
		 bnValEd.setTextFill(marron);
		 
		//font
		 try {
			 final Font f = Font.loadFont(new FileInputStream(new File("./src/font/titre.otf")), 40);
			 reserv.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("./src/font/texte.ttf")), 25);
			 nomEnt.setFont(f2);
			 nomEntField.setFont(f2);
			 addrEnt.setFont(f2);
			 addrEntField.setFont(f2);
			 addrMailEnt.setFont(f2);
			 addrMailEntField.setFont(f2);
			 numPan.setFont(f2);
			 //numPanField.setFont(f2);
			 resumePan.setFont(f2);
			 resumePanField.setFont(f2);
			 dateDebut.setFont(f2);
			 dateFin.setFont(f2);
			 bnValEd.setFont(f2);
			 bnValEd.setOnAction(e ->{reserver(e,calResDeb,calResFin);});
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 ArrayList<Integer> choixNum = new ArrayList<Integer>();
		 for (Iterator<Integer> iterator = Main.renvoyerNumPanneau().iterator(); iterator.hasNext();) {
			choixNum.add(iterator.next());
		 
		}
		 numPanField.setItems(FXCollections.observableArrayList(choixNum));
		 
		 try
		 {
			 
		//image
		 Image logoR = new Image(new FileInputStream(new File("./src/img/logoR.PNG")));
		 ImageView logoHead = new ImageView(logoR);
		 logoHead.setFitHeight(150);
		 logoHead.setPreserveRatio(true);
		 
		 Image fond = new Image(new FileInputStream(new File("./src/img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);

		 
		//position element
		 
		 	//fond d'ecran
		 fondEcran.setLayoutY(150);
		 
	 		//Mes gridPanes et ScrollPanes
	 GridPane 	gpAll		  = new GridPane();
	 
	 GridPane 	gpBody 		  = new GridPane();
	 GridPane	gpBodyQues	  = new GridPane();

	 
	 		//gpAll
	 RowConstraints row0 = new RowConstraints();
	 row0.setPercentHeight(21);
	 RowConstraints row1 = new RowConstraints();
	 row1.setPercentHeight(79);
	 gpAll.getRowConstraints().addAll(row0, row1);
	 
	 gpAll.add(entete.getGpHead(),0,0);
	 gpAll.add(gpBody, 0, 1);
	 
	 		//gpBody
	 gpBody.setAlignment(Pos.CENTER);
	 GridPane.setHalignment(reserv, HPos.LEFT);
	 GridPane.setHalignment(gpBodyQues, HPos.LEFT);
	 reserv.setPadding(new Insets(0,0,0,500));
	 
	 RowConstraints BodyRow0 = new RowConstraints();
	 BodyRow0.setPercentHeight(15);
	 RowConstraints BodyRow1 = new RowConstraints();
	 BodyRow1.setPercentHeight(85);
	 gpBody.getRowConstraints().addAll(BodyRow0, BodyRow1);
	 
	 ColumnConstraints BodyCol = new ColumnConstraints();
	 BodyCol.setPercentWidth(100);
	 gpBody.getColumnConstraints().addAll(BodyCol);
	 
	 gpBody.add(reserv, 0, 0);
	 gpBody.add(gpBodyQues, 0, 1);
	 
	 		//gpBodyQues
	 gpBody.setHalignment(bnValEd, HPos.RIGHT);
	 
	 ColumnConstraints ColQues0 = new ColumnConstraints();
	 ColQues0.setPercentWidth(30);
	 ColumnConstraints ColQues1 = new ColumnConstraints();
	 ColQues1.setPercentWidth(30);
	 gpBodyQues.getColumnConstraints().addAll(ColQues0, ColQues1);
	 
	 RowConstraints RowQues0 = new RowConstraints();
	 RowQues0.setPercentHeight(13);
	 RowConstraints RowQues1 = new RowConstraints();
	 RowQues1.setPercentHeight(13);
	 RowConstraints RowQues2 = new RowConstraints();
	 RowQues2.setPercentHeight(13);
	 RowConstraints RowQues3 = new RowConstraints();
	 RowQues3.setPercentHeight(13);
	 RowConstraints RowQues4 = new RowConstraints();
	 RowQues4.setPercentHeight(13);
	 RowConstraints RowQues5 = new RowConstraints();
	 RowQues5.setPercentHeight(13);
	 RowConstraints RowQues6 = new RowConstraints();
	 RowQues6.setPercentHeight(13);
	 RowConstraints RowQues7 = new RowConstraints();
	 RowQues7.setPercentHeight(13);
	 gpBodyQues.getRowConstraints().addAll(RowQues0, RowQues1, RowQues2, RowQues3, RowQues4, RowQues5, RowQues6,RowQues7);
	 
	 gpBodyQues.setAlignment(Pos.BASELINE_LEFT);
	 gpBodyQues.setPadding(new Insets(0,0,0,75));
	 gpBodyQues.add(nomEnt, 0, 0);
	 gpBodyQues.add(nomEntField, 1, 0);
	 
	 gpBodyQues.add(addrEnt, 0, 1);
	 gpBodyQues.add(addrEntField, 1, 1);
	 
	 gpBodyQues.add(addrMailEnt, 0, 2);
	 gpBodyQues.add(addrMailEntField, 1, 2);
	 
	 gpBodyQues.add(numPan, 0, 3);
	 gpBodyQues.add(numPanField, 1, 3);
	 
	 gpBodyQues.add(resumePan, 0, 4);
	 gpBodyQues.add(resumePanField, 1, 4);
	 
	 gpBodyQues.add(dateDebut, 0, 5);
	 gpBodyQues.add(calResDeb, 1, 5);
	 
	 gpBodyQues.add(dateFin, 0, 6);
	 gpBodyQues.add(calResFin, 1, 6);
	 
	 gpBodyQues.add(bnValEd, 3, 7);
	 
	//group
	Group header = new Group();
	header.getChildren().addAll(fondEcran, gpAll);
		return header;
		 }
		 catch(FileNotFoundException e)
		 {
			 System.out.println("image non trouvée");
			 return null;
		 }
	}
 

 /**
  * Récupère les informations saisies et crée la demande
  * @param e evenement du clic sur le bouton valider
  * @param CalDeb date de début choisie dans le calendrier
  * @param CalFin date de fin choisie dans le calendrier
  **/
 private void reserver(ActionEvent e,DatePicker CalDeb,DatePicker CalFin)
 {
	 try
	 {
		 if (Main.getTabPanneau().get(numPanField.getSelectionModel().getSelectedItem()).disponible(Date.valueOf(CalDeb.getValue()), Date.valueOf(CalFin.getValue())))
		 {
			 this.laDemande = new Demande(nomEntField.getText(),Date.valueOf(CalDeb.getValue()),Date.valueOf(CalFin.getValue()));
			 Main.associerPanneau(laDemande,numPanField.getSelectionModel().getSelectedItem());
			 this.laDemande.setResume(resumePanField.getText());
			 Main.ajouterDemande(this.getLaDemande());
		 }
		 else
		 {
			 throw new IOException();
		 }
	 }
	 catch(IOException e1)
	 {
		 Alert alert = new Alert(AlertType.INFORMATION, "Date indisponible");
		 alert.showAndWait();
	 }
	 catch(NumberFormatException e2)
	 {
		 Alert alert = new Alert(AlertType.INFORMATION, "Format du numéro de panneau invalide");
		 alert.showAndWait();
	 }
}
 /**
  * 
  * @return la demande
  */
 public Demande getLaDemande()
 {
	 return this.laDemande;
 }
 
 }
 
