package graphique;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.Test;

import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;

public class FenReservationTest {

	@Test
	public void testCreerContenuReservation() {
		HashSet<Integer> tabPanneau = new HashSet<Integer>();
		tabPanneau.add(Integer.valueOf(1));
		tabPanneau.add(Integer.valueOf(2));
		tabPanneau.add(Integer.valueOf(3));
		ChoiceBox<Integer> numPanField = new ChoiceBox<Integer>();
		ArrayList<Integer> choixNum = new ArrayList<Integer>();
		 for (Iterator<Integer> iterator = tabPanneau.iterator(); iterator.hasNext();) {
			choixNum.add(iterator.next());
		 
		}
		
		 numPanField.setItems(FXCollections.observableArrayList(choixNum));
		 System.out.println(numPanField.getItems().size());
		assertTrue(numPanField.getItems().size()==choixNum.size());
	}

}
