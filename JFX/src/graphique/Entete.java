package graphique;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

public class Entete {
	/**
	 * Slogan de l'entreprise
	 */
	private Label		titre		= new Label("Soyez	vous !	 Soyez	 vu !");
	/**
	 * fond couleur du header
	 */
	private Rectangle	head		= new Rectangle(1200, 150);
	/**
	 * gpHead
	 * @throws FileNotFoundException
	 */
	 private GridPane gpHead;

	
	public Entete(){
		//color
		 Color rose = Color.web("#ff0046");
		 head.setFill(rose);
		 titre.setTextFill(Color.WHITE);
		 
		//font
		 try {
			 File titreFontFile = new File("./src/font/titre.otf");
			final Font f = Font.loadFont(new FileInputStream(titreFontFile), 40);
			 titre.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		//image
		 try {
		 Image logoR = new Image(new FileInputStream(new File("./src/img/logoR.PNG")));
		 
		 ImageView logoHead = new ImageView(logoR);
		 logoHead.setFitHeight(150);
		 logoHead.setPreserveRatio(true); 
		 
		 gpHead = new GridPane();
		 GridPane gpContenuHead = new GridPane();
		 
		//gpHead
		 gpHead.add(head, 0, 0);
		 gpHead.add(gpContenuHead, 0, 0);
		 
		 		//gpContenuHead
		 GridPane.setHalignment(titre, HPos.CENTER);
		 
		 ColumnConstraints Headcol0 =new ColumnConstraints();
		 Headcol0.setPercentWidth(15);
		 ColumnConstraints Headcol1 =new ColumnConstraints();
		 Headcol1.setPercentWidth(85);
		 gpContenuHead.getColumnConstraints().addAll(Headcol0, Headcol1);
		 
		 gpContenuHead.add(logoHead, 0, 0);
		 gpContenuHead.add(titre, 1, 0);
		 }
		 catch(FileNotFoundException e)
		 {
			 System.out.println("image non trouvée");
		 }
	}
	
	public GridPane getGpHead(){
		return this.gpHead;
	}
}
