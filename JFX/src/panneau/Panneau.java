package panneau;



import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import panneau.Emplacement;

public class Panneau {
	private int numPanneau;
	private double longueur;
	private double largeur;
	private Emplacement localisation;
	private HashMap<Date, Date> tabDate;
	private String resume;

	/**
	 * Construit le panneau
	 * 
	 * @param lon
	 *            longueur en m
	 * @param lar
	 *            largeur en m
	 * @param loc
	 *            localisation du panneau
	 */
	public Panneau(int num,double lon, double lar, Emplacement loc)
	{
		numPanneau=num;
		longueur = lon;
		largeur = lar;
		localisation = loc;
		tabDate = new HashMap<Date, Date>();
		
	}
	
	

	/**
	 * 
	 * @return prix de Location en fonction de la longueur et la largeur
	 */
	public double prixLoc()//
	{
		return this.longueur * this.largeur * 1.7;
	}

	/**
	 * 
	 * @return si le panneau est disponible
	 */
	public boolean disponible(Date debut,Date fin)
	{
		boolean dispo = true;
		for (Date d : tabDate.keySet()) 
		{
			Date f = tabDate.get(d);
			
			if (
					(
							(debut.after(d)|| Panneau.estEgale(debut, d)) && (debut.before(f)||Panneau.estEgale(debut, f))
						) 
				|| (
							(fin.after(f) || Panneau.estEgale(fin, d)) && (fin.before(f) ||Panneau.estEgale(fin, f))
						) 
				
				|| 
				
					(
							(debut.before(d) || Panneau.estEgale(debut, d)) && (fin.after(f) || Panneau.estEgale(fin, f))
						)
						
				)
				
			{
				dispo = false;
				break;
				
			}
			
			
		}
		return dispo;
	}

	/**
	 * Rend le panneau non disponible
	 */
	public void reserver(Date debut, Date fin) 
	{
			tabDate.putIfAbsent(debut, fin);
	}

	/**
	 * Rend le panneau disponible
	 */
	public void liberer(Date debut) 
	{
		if (tabDate.containsValue(debut))
		{
			tabDate.remove(debut);
		}
	
	}

	/**
	 * 
	 * @return localisation
	 */
	public Emplacement getLocalisation() 
	{
		return this.localisation;
	}



	/**
	 * @return the resume
	 */
	public String getResume() {
		return resume;
	}



	/**
	 * @param resume the resume to set
	 */
	public void setResume(String resume) {
		this.resume = resume;
	}
	
	/**
	 * @return le numéro du panneau
	 */
	public int getnumPanneau() {
		return numPanneau;
	}
	
	/**
	 * 
	 * @return tabDate le tableau des dates réservées du panneau
	 */
	public HashMap<Date,Date> getTabDate()
	{
		return tabDate;
	}
	
	/**
	 * Détermine si les deux dates sont les mêmes au jour près
	 * @param d date de début
	 * @param f date de fin
	 * @return vrai si les mêmes, faux sinon
	 */
	public static boolean estEgale(Date d,Date f)
	{
		return d.getYear()==f.getYear() && d.getMonth() == f.getMonth() && d.getDate() == f.getDate();
	}
}
