package panneau;



public class Emplacement 
{
	private double x;
	private double y;
	/**
	 * Construit emplacement
	 * @param x1 abscisse
	 * @param y1 ordonnée
	 */
	public Emplacement(double x1,double y1)
	{
		setX(x1);
		setY(y1);
	}
	/**
	 * 
	 * @return l'abscisse x
	 */
	public double getX() {
		return x;
	}
	/**
	 * Modifie l'abscisse par x
	 * @param x l'abscisse à mettre
	 */
	public void setX( double x) 
	{
		this.x = x;
	}
	/**
	 * 
	 * @return l'ordonnée y
	 */
	public double getY() {
		return y;
	}
	/**
	 * Modifie l'ordonnée par y
	 * @param y l'ordonnée à mettre
	 */
	public void setY(double y) {
		this.y = y;
	}
	
	
	
}
