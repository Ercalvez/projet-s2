package panneau;


import java.util.Date;

import panneau.Emplacement;

public class Demande 
{
	private String nom;
	private Panneau panneau;
	private Date debut;
	private Date fin;
	
	/**
	 * Construit la demande
	 * @param id l'identifiant de l'entreprise
	 * @param n le nom de l'entreprise
	 */
	public Demande(String n,Date d,Date f)
	{
		nom=n;
		debut=d;
		fin=f;
		panneau=null;
	}
	/**
	 * Construit la demande et effectue la réservation
	 * @param id l'identifiant de l'entreprise
	 * @param n le nom de l'entreprise
	 * @param p le panneau à réserver
	 */
	public Demande(String n,Panneau p,Date d,Date f)
	{
		nom=n;
		debut=d;
		fin=f;
		panneau=p;
		
	}
	
	//TODO réservation depuis l'interface
	public void reservation(Panneau p)
	{
		reserverPanneau(p,debut,fin);
	}
	
	/**
	 * réserve la panneau
	 * @param p le panneau à réserver
	 */
	public void reserverPanneau(Panneau p,Date d,Date f)
	{
		if (p.disponible(d,f))
		{
			this.setPanneau(p);
			p.reserver(d,f);
		}

	}
	
	/**
	 * 
	 * @return panneau
	 */
	public Panneau getPanneau()
	{
		return panneau;
	}
	
	/**
	 * 
	 * @param p le panneau à mettre
	 */
	public void setPanneau(Panneau p)
	{
		this.panneau = p;
	}
	
	public String getNom() 
	{
		return nom;
	}
	
	public void setNom(String nom) 
	{
		this.nom = nom;
	}
	
	public Date getDebut()
	{
		return this.debut;
	}
	

	public Date getFin()
	{
		return this.fin;
	}
	
	public String getResume()
	{
		return this.getPanneau().getResume();
	}
	
	public void setResume(String r)
	{
		this.panneau.setResume(r);
	}
	
}
