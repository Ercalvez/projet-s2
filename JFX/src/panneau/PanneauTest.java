package panneau;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;


public class PanneauTest
{



	@Test
	public void testDisponible() 
	{
		Panneau p = new Panneau(1, 1.0, 1.0, new Emplacement(20, 20));
		p.reserver(new Date(),new Date());
		assertEquals(p.disponible(new Date(),new Date()),false);

	}


	@Test
	public void testEstEgale() 
	{
		assertTrue(Panneau.estEgale(new Date(),new Date()));
	}

}
