import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.time.LocalDate;

import com.sun.javafx.css.converters.StringConverter;

import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Fen1 extends Stage {
	/**
	 * Titre des cat�gories pour orienter le client 
	 */
	private Label		mairie		= new Label("Mairie");
	private Label		entreprise	= new Label("Entreprise");
	private Label		carte		= new Label("Carte");
	private Label		reserv		= new Label("Reservation");
	private Label		demandes	= new Label("Demandes");
	
	/**
	 * Orienter le client s'il vient de la mairie
	 */
	private Label		loginMai	= new Label("Entrez votre login de l'entreprise :");
	/**
	 * Entrer du login
	 */
	private TextField login 		= new TextField();
	/**
	 * bouton pour valider, �diter une demande, afficherla carte
	 */
	private Button		bnEditer	= new Button("Editer une demande");
	private Button		bnCarte		= new Button("Afficher la carte");
	private Button		bnValide	= new Button("Valider");
	
	/**
	 * texte et image de la page carte
	 */
	private Rectangle	imgCarte	= new Rectangle(550, 625);
	private Label		panLibre	= new Label("Panneau libre");
	private Label		panPrit		= new Label("Panneau réservé");
	private Label		detailsPan	= new Label("Cliquez sur un panneau pour plus de détails.");
	
	/**
	 * elements pour la page reservation
	 */ 
	private Label 		nomEnt			 = new Label("Nom de l'entreprise :");
	private TextField	nomEntField	 	 = new TextField();
	private Label 		addrEnt			 = new Label("Adresse (localisation) :");
	private TextField	addrEntField	 = new TextField();
	private Label 		addrMailEnt		 = new Label("Adresse Mail :");
	private TextField	addrMailEntField = new TextField();
	private Label		numPan			 = new Label("Numéro du panneau sélectionné :");
	private TextField	numPanField 	 = new TextField();
	private Label		resumePan		 = new Label("Résumé de la publicité :");
	private TextField	resumePanField 	 = new TextField();
	private Label		dateDebut		 = new Label("Date de début de la réservation :");
	private Label		dateFin			 = new Label("Date de fin de la réservation :");
	private Button		bnValEd			 = new Button("Valider");
	
	/**
	 * Constructeur de la fenetre, fait appel a creerContenu()
	 * @throws FileNotFoundException
	 */
	public Fen1() throws FileNotFoundException{
		//title
		this.setTitle("SEE-GNBOARG");
		
		//size
		 this.setWidth(1500);
		 this.setHeight(800);
		 this.setResizable(false);
		 
		 //position
		 this.setX(50);
		 this.setY(20);
		 
		 //scene
		 Scene sc1 = new Scene(creerContenuDemande(),1200,800);
		 this.setScene(sc1);
		 this.sizeToScene();
	}
	
	/**
	 * Creer le contenu de la Page d'accueil
	 * @return elements
	 * @throws FileNotFoundException
	 */
	Parent creerContenuAccueil() throws FileNotFoundException {
		// appel entete
		Entete entete = new Entete();
		
		//color
		 Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 mairie.setTextFill(orange);
		 entreprise.setTextFill(orange);
		 loginMai.setTextFill(marron);
		 bnEditer.setTextFill(marron);
		 bnCarte.setTextFill(marron);
		 bnValide.setTextFill(marron);
		 
		 
		 //font
		 try {
			 final Font f = Font.loadFont(new FileInputStream(new File("./font/titre.otf")), 40);
			 mairie.setFont(f);
			 entreprise.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("./font/texte.ttf")), 25);
			 loginMai.setFont(f2);
			 bnEditer.setFont(f2);
			 bnCarte.setFont(f2);
			 bnValide.setFont(f2);
			 login.setFont(f2);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 //image 
		 Image barre = new Image(new FileInputStream(new File("./img/barre.png")));
		 ImageView barreO = new ImageView(barre);
		 barreO.setFitHeight(550);
		 barreO.setPreserveRatio(true);
		 
		 Image fond = new Image(new FileInputStream(new File("./img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);
		
		//position element
		 
		 		//fond
		 fondEcran.setLayoutX(0);
		 fondEcran.setLayoutY(150);
		 
		 		//Mes gridPane
		 GridPane gpAll			= new GridPane();
		 
		 GridPane gpBody 		= new GridPane();
		 GridPane gpBodyMairie	= new GridPane();
		 GridPane gpBodyEnt		= new GridPane();

		 
		 		//gpAll
		 RowConstraints row0 = new RowConstraints();
		 row0.setPercentHeight(25);
		 RowConstraints row1 = new RowConstraints();
		 row1.setPercentHeight(75);
		 gpAll.getRowConstraints().addAll(row0, row1);
		 
		 gpAll.add(entete.getGpHead(),0,0);
		 gpAll.add(gpBody, 0, 1);
		 
		 		
		 		//gpBody
		 gpBody.setAlignment(Pos.BASELINE_LEFT);
		 gpBody.setHalignment(gpBodyMairie, HPos.LEFT);
		 gpBody.setHalignment(barreO, HPos.LEFT);
		 gpBody.setHalignment(gpBodyEnt, HPos.LEFT);
		 ColumnConstraints CBodyCol0 =new ColumnConstraints();
		 CBodyCol0.setPercentWidth(40);
		 ColumnConstraints CBodyCol1 =new ColumnConstraints();
		 CBodyCol1.setPercentWidth(6);
		 ColumnConstraints CBodyCol2 =new ColumnConstraints();
		 CBodyCol2.setPercentWidth(50);
		 gpBody.getColumnConstraints().addAll(CBodyCol0, CBodyCol1, CBodyCol2);
		 
		 gpBody.add(gpBodyMairie, 0, 0);
		 gpBody.add(barreO, 1, 0);
		 gpBody.add(gpBodyEnt, 2, 0);
		 
		 		//gpBodyMairie
		 gpBodyMairie.setAlignment(Pos.BASELINE_LEFT);
		 gpBodyMairie.setHalignment(mairie, HPos.CENTER);
		 gpBodyMairie.setHalignment(loginMai, HPos.LEFT);
		 gpBodyMairie.setHalignment(login, HPos.LEFT);
		 gpBodyMairie.setHalignment(bnValide, HPos.RIGHT);
		 gpBodyMairie.setPadding(new Insets(0,0,0,30));
		 
		 RowConstraints CBodyMairieRow0 =new RowConstraints();
		 CBodyMairieRow0.setPercentHeight(20);
		 RowConstraints CBodyMairieRow1 =new RowConstraints();
		 CBodyMairieRow1.setPercentHeight(25);
		 RowConstraints CBodyMairieRow2 =new RowConstraints();
		 CBodyMairieRow2.setPercentHeight(25);
		 RowConstraints CBodyMairieRow3 =new RowConstraints();
		 CBodyMairieRow3.setPercentHeight(30);
		 gpBodyMairie.getRowConstraints().addAll(CBodyMairieRow0, CBodyMairieRow1, CBodyMairieRow2, CBodyMairieRow3);

		 gpBodyMairie.add(mairie,  0,  0);
		 gpBodyMairie.add(loginMai, 0, 1);
		 gpBodyMairie.add(login, 0, 2);
		 gpBodyMairie.add(bnValide, 0, 3);
		 
		 		//gpBodyEnt
		 gpBodyEnt.setAlignment(Pos.BASELINE_LEFT);
		 gpBodyEnt.setHalignment(entreprise, HPos.CENTER);
		 gpBodyEnt.setHalignment(bnEditer, HPos.CENTER);
		 gpBodyEnt.setHalignment(bnCarte, HPos.CENTER);
		 gpBodyEnt.setPadding(new Insets(0,0,0,100));
		 
		 RowConstraints CBodyEntRow0 =new RowConstraints();
		 CBodyEntRow0.setPercentHeight(20);
		 RowConstraints CBodyEntRow1 =new RowConstraints();
		 CBodyEntRow1.setPercentHeight(40);
		 RowConstraints CBodyEntRow2 =new RowConstraints();
		 CBodyEntRow2.setPercentHeight(40);
		 gpBodyEnt.getRowConstraints().addAll(CBodyEntRow0, CBodyEntRow1, CBodyEntRow2);
		 
		 gpBodyEnt.add(entreprise, 0,0);
		 gpBodyEnt.add(bnEditer, 0, 1);
		 gpBodyEnt.add(bnCarte,  0,  2);
		 
		 //action bouton
		 bnEditer.setOnAction(e -> {try {
			gererClic(e);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}});
		 bnCarte.setOnAction(e -> {try {
			gererClic(e);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}});

		//group
		Group header = new Group();
		header.getChildren().addAll(fondEcran, gpAll);
		return header;
	}
	

	/**
	 * Creer le contenu de la Page Carte
	 * @return elements
	 * @throws FileNotFoundException
	 */
	 Parent creerContenuCarte() throws FileNotFoundException {
		// appel entete
			Entete entete = new Entete();
			
		//color
		 Color rose = Color.web("#ff0046");
		 Color orange = Color.web("#ffa200");
		 Color marron = Color.web("#664040");
		 carte.setTextFill(orange);
		 panLibre.setTextFill(marron);
		 panPrit.setTextFill(marron);
		 detailsPan.setTextFill(marron);
		 bnEditer.setTextFill(marron);
		 imgCarte.setFill(marron);
		 
		//font
		 try {
			 final Font f = Font.loadFont(new FileInputStream(new File("./font/titre.otf")), 40);
			 carte.setFont(f);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 try {
			 final Font f2 = Font.loadFont(new FileInputStream(new File("./font/texte.ttf")), 25);
			 panLibre.setFont(f2);
			 panPrit.setFont(f2);
			 detailsPan.setFont(f2);
			 bnEditer.setFont(f2);
		 }catch (FileNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		//image
		 Image fond = new Image(new FileInputStream(new File("./img/fond.PNG")));
		 ImageView fondEcran = new ImageView(fond);
		 fondEcran.setFitHeight(650);
		 fondEcran.setPreserveRatio(true);
		 
		 Image PanneauPrit = new Image(new FileInputStream(new File("./img/iconePrise.png")));
		 ImageView Pprit = new ImageView(PanneauPrit);
		 Pprit.setFitHeight(75);
		 Pprit.setPreserveRatio(true);
		 
		 Image PanneauLibre = new Image(new FileInputStream(new File("./img/iconeLibre.png")));
		 ImageView Plibre = new ImageView(PanneauLibre);
		 Plibre.setFitHeight(75);
		 Plibre.setPreserveRatio(true);
		 
		//position element
		 
		 	//fond d'ecran
		 fondEcran.setLayoutY(150);
		 
	 		//Mes gridPane
	 GridPane gpAll			= new GridPane();
	 
	 GridPane gpBody 		  = new GridPane();
	 GridPane gpBodyGauche	  = new GridPane();
	 GridPane gpBodyDroit	  = new GridPane();
	 GridPane gpBodyPanneauLi = new GridPane();
	 GridPane gpBodyPanneauPr = new GridPane();

	 
	 		//gpAll
	 RowConstraints row0 = new RowConstraints();
	 row0.setPercentHeight(20.5);
	 RowConstraints row1 = new RowConstraints();
	 row1.setPercentHeight(79.5);
	 gpAll.getRowConstraints().addAll(row0, row1);
	 
	 gpAll.add(entete.getGpHead(),0,0);
	 gpAll.add(gpBody, 0, 1);
	 
	 		//gpBody
	 gpBody.setAlignment(Pos.CENTER);
	 gpBody.setHalignment(gpBodyGauche, HPos.CENTER);
	 gpBody.setHalignment(gpBodyDroit, HPos.CENTER);
	 
	 ColumnConstraints CBodyCol0 =new ColumnConstraints();
	 CBodyCol0.setPercentWidth(45);
	 ColumnConstraints CBodyCol1 =new ColumnConstraints();
	 CBodyCol1.setPercentWidth(55);
	 gpBody.getColumnConstraints().addAll(CBodyCol0, CBodyCol1);
	 
	 gpBody.add(gpBodyGauche, 0, 0);
	 gpBody.add(gpBodyDroit, 1, 0);
	 
	 		//gpBodyGauche
	 gpBodyGauche.setAlignment(Pos.BASELINE_LEFT);
	 gpBodyGauche.setHalignment(carte, HPos.CENTER);
	 gpBodyDroit.setHalignment(gpBodyPanneauLi, HPos.LEFT);
	 gpBodyDroit.setHalignment(gpBodyPanneauPr, HPos.LEFT);
	 gpBodyDroit.setHalignment(bnEditer, HPos.RIGHT);
	 gpBodyGauche.setPadding(new Insets(0,0,0,25));
	 
	 RowConstraints BodyGaucheRow0 =new RowConstraints();
	 BodyGaucheRow0.setPercentHeight(30);
	 RowConstraints BodyGaucheRow1 =new RowConstraints();
	 BodyGaucheRow1.setPercentHeight(15);
	 RowConstraints BodyGaucheRow2 =new RowConstraints();
	 BodyGaucheRow2.setPercentHeight(15);
	 RowConstraints BodyGaucheRow3 =new RowConstraints();
	 BodyGaucheRow3.setPercentHeight(15);
	 RowConstraints BodyGaucheRow4 =new RowConstraints();
	 BodyGaucheRow4.setPercentHeight(25);
	 gpBodyGauche.getRowConstraints().addAll(BodyGaucheRow0, BodyGaucheRow1, BodyGaucheRow2, BodyGaucheRow3, BodyGaucheRow4);
			 
	 gpBodyGauche.add(carte,  0,  0);
	 gpBodyGauche.add(gpBodyPanneauLi, 0, 1);
	 gpBodyGauche.add(gpBodyPanneauPr, 0, 2);
	 gpBodyGauche.add(detailsPan, 0, 3);
	 gpBodyGauche.add(bnEditer, 0, 4);
	 
	 		//gpBodyDroite
	 gpBodyDroit.setAlignment(Pos.BASELINE_LEFT);
	 gpBodyDroit.setHalignment(imgCarte, HPos.LEFT);
	 
	 gpBodyDroit.add(imgCarte, 0,0);
	 
	 		//gpBodyPanneauLi
	 gpBodyPanneauLi.setHalignment(Plibre, HPos.LEFT);
	 gpBodyPanneauLi.setHalignment(panLibre, HPos.LEFT);
	 
	 ColumnConstraints BodyPanCol0 =new ColumnConstraints();
	 BodyPanCol0.setPercentWidth(15);
	 ColumnConstraints BodyPanCol1 =new ColumnConstraints();
	 BodyPanCol1.setPercentWidth(85);
	 gpBodyPanneauLi.getColumnConstraints().addAll(BodyPanCol0, BodyPanCol1);
	 
	 gpBodyPanneauLi.add(Plibre, 0, 0);
	 gpBodyPanneauLi.add(panLibre, 1, 0);
	 
	 		//gpBodyPanneauPr
	 gpBodyPanneauPr.setHalignment(Pprit, HPos.LEFT);
	 gpBodyPanneauPr.setHalignment(panPrit, HPos.LEFT);

	 
	 gpBodyPanneauPr.getColumnConstraints().addAll(BodyPanCol0, BodyPanCol1);
	 
	 gpBodyPanneauPr.add(Pprit, 0, 0);
	 gpBodyPanneauPr.add(panPrit, 1, 0);
	 
	//action bouton
	 bnEditer.setOnAction(e -> {try {
		gererClic(e);
	} catch (FileNotFoundException e1) {
		e1.printStackTrace();
	}});
	 
	//group
	Group header = new Group();
	header.getChildren().addAll(fondEcran, gpAll);
		return header;
	}

	 /**
	  * Creer le contenu de la page Reservation
	  * @return
	  * @throws FileNotFoundException
	  */
	 Parent creerContenuReservation() throws FileNotFoundException {
		 	// appel entete
			Entete entete = new Entete();
		 
			//gestion des calendriers de reservation
			 DatePicker calResDeb = new DatePicker();
			 calResDeb.setValue(LocalDate.now());
			 calResDeb.setShowWeekNumbers(true);
			 
			 DatePicker calResFin = new DatePicker();
			 calResFin.setValue(calResDeb.getValue());
			 calResFin.setShowWeekNumbers(true);
			 
			//color
			 Color rose = Color.web("#ff0046");
			 Color orange = Color.web("#ffa200");
			 Color marron = Color.web("#664040");
			 reserv.setTextFill(orange);
			 nomEnt.setTextFill(marron);
			 addrEnt.setTextFill(marron);
			 addrMailEnt.setTextFill(marron);
			 numPan.setTextFill(marron);
			 resumePan.setTextFill(marron);
			 dateDebut.setTextFill(marron);
			 dateFin.setTextFill(marron);
			 bnValEd.setTextFill(marron);
			 
			//font
			 try {
				 final Font f = Font.loadFont(new FileInputStream(new File("./font/titre.otf")), 40);
				 reserv.setFont(f);
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			 
			 try {
				 final Font f2 = Font.loadFont(new FileInputStream(new File("./font/texte.ttf")), 25);
				 nomEnt.setFont(f2);
				 nomEntField.setFont(f2);
				 addrEnt.setFont(f2);
				 addrEntField.setFont(f2);
				 addrMailEnt.setFont(f2);
				 addrMailEntField.setFont(f2);
				 numPan.setFont(f2);
				 numPanField.setFont(f2);
				 resumePan.setFont(f2);
				 resumePanField.setFont(f2);
				 dateDebut.setFont(f2);
				 dateFin.setFont(f2);
				 bnValEd.setFont(f2);
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			 
			//image
			 Image fond = new Image(new FileInputStream(new File("./img/fond.PNG")));
			 ImageView fondEcran = new ImageView(fond);
			 fondEcran.setFitHeight(650);
			 fondEcran.setPreserveRatio(true);
	
			 
			//position element
			 
			 	//fond d'ecran
			 fondEcran.setLayoutY(150);
			 
		 		//Mes gridPane et ScrollPane
		 GridPane 	gpAll		  = new GridPane();
		 
		 GridPane 	gpBody 		  = new GridPane();
		 GridPane	gpBodyQues	  = new GridPane();

		 
		 		//gpAll
		 RowConstraints row0 = new RowConstraints();
		 row0.setPercentHeight(21);
		 RowConstraints row1 = new RowConstraints();
		 row1.setPercentHeight(79);
		 gpAll.getRowConstraints().addAll(row0, row1);
		 
		 gpAll.add(entete.getGpHead(),0,0);
		 gpAll.add(gpBody, 0, 1);
		 
		 		//gpBody
		 gpBody.setAlignment(Pos.CENTER);
		 gpBody.setHalignment(reserv, HPos.LEFT);
		 gpBody.setHalignment(gpBodyQues, HPos.LEFT);
		 reserv.setPadding(new Insets(0,0,0,500));
		 
		 RowConstraints BodyRow0 = new RowConstraints();
		 BodyRow0.setPercentHeight(15);
		 RowConstraints BodyRow1 = new RowConstraints();
		 BodyRow1.setPercentHeight(85);
		 gpBody.getRowConstraints().addAll(BodyRow0, BodyRow1);
		 
		 ColumnConstraints BodyCol = new ColumnConstraints();
		 BodyCol.setPercentWidth(100);
		 gpBody.getColumnConstraints().addAll(BodyCol);
		 
		 gpBody.add(reserv, 0, 0);
		 gpBody.add(gpBodyQues, 0, 1);
		 
		 		//gpBodyQues
		 gpBody.setHalignment(bnValEd, HPos.RIGHT);
		 
		 ColumnConstraints ColQues0 = new ColumnConstraints();
		 ColQues0.setPercentWidth(30);
		 ColumnConstraints ColQues1 = new ColumnConstraints();
		 ColQues1.setPercentWidth(30);
		 gpBodyQues.getColumnConstraints().addAll(ColQues0, ColQues1);
		 
		 
		 RowConstraints RowQues0 = new RowConstraints();
		 RowQues0.setPercentHeight(13);
		 RowConstraints RowQues1 = new RowConstraints();
		 RowQues1.setPercentHeight(13);
		 RowConstraints RowQues2 = new RowConstraints();
		 RowQues2.setPercentHeight(13);
		 RowConstraints RowQues3 = new RowConstraints();
		 RowQues3.setPercentHeight(13);
		 RowConstraints RowQues4 = new RowConstraints();
		 RowQues4.setPercentHeight(13);
		 RowConstraints RowQues5 = new RowConstraints();
		 RowQues5.setPercentHeight(13);
		 RowConstraints RowQues6 = new RowConstraints();
		 RowQues6.setPercentHeight(13);
		 RowConstraints RowQues7 = new RowConstraints();
		 RowQues7.setPercentHeight(13);
		 gpBodyQues.getRowConstraints().addAll(RowQues0, RowQues1, RowQues2, RowQues3, RowQues4, RowQues5, RowQues6,RowQues7);
		 
		 gpBodyQues.setAlignment(Pos.BASELINE_LEFT);
		 gpBodyQues.setPadding(new Insets(0,0,0,75));
		 gpBodyQues.add(nomEnt, 0, 0);
		 gpBodyQues.add(nomEntField, 1, 0);
		 
		 gpBodyQues.add(addrEnt, 0, 1);
		 gpBodyQues.add(addrEntField, 1, 1);
		 
		 gpBodyQues.add(addrMailEnt, 0, 2);
		 gpBodyQues.add(addrMailEntField, 1, 2);
		 
		 gpBodyQues.add(numPan, 0, 3);
		 gpBodyQues.add(numPanField, 1, 3);
		 
		 gpBodyQues.add(resumePan, 0, 4);
		 gpBodyQues.add(resumePanField, 1, 4);
		 
		 gpBodyQues.add(dateDebut, 0, 5);
		 gpBodyQues.add(calResDeb, 1, 5);
		 
		 gpBodyQues.add(dateFin, 0, 6);
		 gpBodyQues.add(calResFin, 1, 6);
		 
		 gpBodyQues.add(bnValEd, 1, 7);
		 
		//group
		Group header = new Group();
		header.getChildren().addAll(fondEcran, gpAll);
			return header;
		}
/**
 * Creer le contenu de la demande, côté Mairie	 
 * @return
 * @throws FileNotFoundException
 */
Parent creerContenuDemande() throws FileNotFoundException {
    	    // appel entete
 	    	Entete entete = new Entete();
 	    	
 	    	//scroll bar
 	    	ScrollPane sc = new ScrollPane();
 	    	sc.setVbarPolicy(ScrollBarPolicy.ALWAYS);
 	    	sc.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
			
 	    	//color
			 Color rose = Color.web("#ff0046");
			 Color orange = Color.web("#ffa200");
			 Color marron = Color.web("#664040");
			 demandes.setTextFill(orange);
			 
			 
			//font
			 try {
				 final Font f = Font.loadFont(new FileInputStream(new File("./font/titre.otf")), 40);
				 demandes.setFont(f);
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			 
			 try {
				 final Font f2 = Font.loadFont(new FileInputStream(new File("./font/texte.ttf")), 25);
			 }catch (FileNotFoundException e) {
				 e.printStackTrace();
			 }
			 
			//image
			 Image fond = new Image(new FileInputStream(new File("./img/fond.PNG")));
			 ImageView fondEcran = new ImageView(fond);
			 fondEcran.setFitHeight(650);
			 fondEcran.setPreserveRatio(true);
	
			 
			//position element
			 
			 	//fond d'ecran
			 fondEcran.setLayoutY(150);
			 
		 		//Mes gridPane et ScrollPane
		 GridPane 	gpAll		  = new GridPane();
		 GridPane 	gpBody 		  = new GridPane();
		 GridPane 	gpBodyScroll  = new GridPane();	
		 
		 		//gpAll
		 RowConstraints row0 = new RowConstraints();
		 row0.setPercentHeight(21);
		 RowConstraints row1 = new RowConstraints();
		 row1.setPercentHeight(79);
		 gpAll.getRowConstraints().addAll(row0, row1);
		 
		 gpAll.add(entete.getGpHead(),0,0);
		 gpAll.add(gpBody, 0, 1);
		 
		 		//gpBody
		 gpBody.setAlignment(Pos.BASELINE_LEFT);
		 gpBody.setHalignment(demandes, HPos.CENTER);
		 gpBody.setHalignment(gpBodyScroll, HPos.LEFT);
		 gpBodyScroll.setPadding(new Insets(0,0,0,50));

		 RowConstraints BodyRow0 = new RowConstraints();
		 BodyRow0.setPercentHeight(15);
		 RowConstraints BodyRow1 = new RowConstraints();
		 BodyRow1.setPercentHeight(85);
		 gpBody.getRowConstraints().addAll(BodyRow0, BodyRow1);
		 
		 ColumnConstraints BodyCol = new ColumnConstraints();
		 BodyCol.setPercentWidth(80);
		 gpBody.getColumnConstraints().addAll(BodyCol);
		 
		 gpBody.add(demandes, 0, 0);
		 gpBody.add(sc, 0, 1);
		 
		 		//gpBodyScroll
		 gpBodyScroll.setAlignment(Pos.BASELINE_LEFT);
		 
		//group
		Group header = new Group();
		header.getChildren().addAll(fondEcran, gpAll);
			return header;
		}
	 

	 /**
	  * Gerer le clic
	  * @param e
	 * @throws FileNotFoundException 
	  */
	 private void gererClic(ActionEvent e) throws FileNotFoundException {
		 if(e.getSource()==bnEditer){
			 Scene sc2 = new Scene(creerContenuReservation(), 1200, 800);
			 this.setScene(sc2);
			 this.sizeToScene();
		 }else if(e.getSource()==bnCarte){
			 Scene sc3 = new Scene(creerContenuCarte(), 1200, 800);
			 this.setScene(sc3);
			 this.sizeToScene();
		 }
	}
}
